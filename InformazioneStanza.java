public class InformazioneStanza extends Informazione{
	private int idAlbergo;
	private int idStanza;
	private String informazione;

	public InformazioneStanza(int idAlbergo, int idStanza, String info){
		this.idAlbergo = idAlbergo;
		this.idStanza = idStanza;
		this.informazione = info;
	}
	
	public String getInformazione(){
		return this.informazione;
	}

	public int getIdAlbergo(){
		return this.idAlbergo;
	}

	public int getIdStanza(){
		return this.idStanza;
	}
}
