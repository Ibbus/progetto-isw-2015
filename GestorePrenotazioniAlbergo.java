import java.util.*;

public class GestorePrenotazioniAlbergo{
	private HashMap<String,Stanza> prenotate;
	private ArrayList<Stanza> daPrenotare;

	public GestorePrenotazioniAlbergo(){
		prenotate = new HashMap<String,Stanza>();
		daPrenotare = new ArrayList<Stanza>();
	}

	public boolean giaPrenotato(int room){
		Collection<Stanza> stanzePrenotate = new ArrayList<Stanza>();
		stanzePrenotate = this.prenotate.values();
		for(Stanza s: stanzePrenotate){
			if(s.getId() == room){
				return true;
			}
		}
		return false;
	}

	public boolean contieneDaPrenotare(int idStanza){
		for(Stanza s : this.daPrenotare){
			if(s.getId() == idStanza){
				return true;
			}
		}
		return false;
	}

	public void aggiungiDaPrenotare(int id, String tipo, double prezzo){
		this.daPrenotare.add(new Stanza(id,tipo,prezzo));
	}

	public boolean aggiungiPrenotazione(String key, Integer value){
		boolean prenotata = false; int i = 0;		
		for(Stanza s: this.daPrenotare){
			if(s.getId() == value && !prenotata){
				this.prenotate.put(key,s);
				prenotata = true;
				this.daPrenotare.remove(s);
				break;
			}
		}
		if(!prenotata){
			System.out.println("Stanza non prenotata, non è presente all'interno dell'albergo");
		}
		return prenotata;
	}

	public int numeroStanzePrenotabili(){
		return this.daPrenotare.size();
	}

	public boolean annullaPrenotazione(String nome, int idStanza){
		Collection<Stanza> stanzePrenotate = new ArrayList<Stanza>();
		stanzePrenotate = this.prenotate.values();
		for(Stanza s: stanzePrenotate){
			if(s.getId() == idStanza){
				this.prenotate.remove(nome);
				this.daPrenotare.add(s);
				return true;
			}
		}
		return false;
	}

	public Stanza getStanzePrenotate(String nominativo){
		return this.prenotate.get(nominativo);
	}
}
