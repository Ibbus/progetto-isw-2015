public class InformazioneAlbergo extends Informazione{
	private int idAlbergo;	
	private String informazione;

	public InformazioneAlbergo(int idAlbergo, String info){
		this.idAlbergo = idAlbergo;
		this.informazione = info;
	}
	
	public String getInformazione(){
		return this.informazione;
	}

	public int getIdAlbergo(){
		return this.idAlbergo;
	}
}
