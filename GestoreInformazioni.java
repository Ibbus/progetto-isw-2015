public class GestoreInformazioni{

	public void aggiungiInformazione(int idAlbergo, String info){
		AggregatoreInformazioni.getInstance().addInfo(idAlbergo, info);
	}

	public void aggiungiInformazione(int idAlbergo, int idStanza, String info){
		AggregatoreInformazioni.getInstance().addInfo(idAlbergo, idStanza, info);
	}
}
