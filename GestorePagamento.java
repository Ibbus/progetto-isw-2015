public class GestorePagamento {
    
    public String pagaStanze(Albergo albergo, String nome){
		Stanza room = albergo.getStanzePrenotate(nome);
        return "Hai pagato " + room.getPrezzo() + " euro per le stanze";
    } 
}
