import java.util.*;

public class AggregatoreInformazioni{
	private ArrayList<Informazione>	info;
	private static AggregatoreInformazioni instance;

	private AggregatoreInformazioni(){
		this.info = new ArrayList<Informazione>();
	}

	public static AggregatoreInformazioni getInstance(){
		if(instance == null){
			instance = new AggregatoreInformazioni();
		}
		return instance;
	}

	public String visualizzaInfo(int idAlbergo){
		for(Informazione infoAlbergo : info){
			if(infoAlbergo.getClass().getCanonicalName().equals("InformazioneAlbergo")){
				if(((InformazioneAlbergo) infoAlbergo).getIdAlbergo() == idAlbergo){
					return ((InformazioneAlbergo) infoAlbergo).getInformazione();
				}
			}
			//return infoAlbergo.getClass().getCanonicalName();
		}
		return "Non sono presenti informazioni riguardanti questo albergo";
	}

	public String visualizzaInfo(int idAlbergo, int idStanza){
		for(Informazione infoStanza : info){
			if(infoStanza.getClass().getCanonicalName().equals("InformazioneStanza")){
				if(((InformazioneStanza) infoStanza).getIdAlbergo() == idAlbergo && ((InformazioneStanza) infoStanza).getIdStanza() == idStanza){
					return ((InformazioneStanza) infoStanza).getInformazione();
				}
			}
			//return infoStanza.getClass().getCanonicalName();
		}
		return "Non sono presenti informazioni riguardanti questa stanza";
	}

	public void addInfo(int idA, String info){
		this.info.add(new InformazioneAlbergo(idA,info));
	}

	public void addInfo(int idA, int idS, String info){
		this.info.add(new InformazioneStanza(idA, idS, info));
	}

	public ArrayList<Informazione> getInfo(){
		return this.info;
	}
}
