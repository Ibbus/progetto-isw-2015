import static org.junit.Assert.*;
import org.junit.Test;

public class Tester_Prenotazioni{

	@Test
	public void GeneralTest(){	
		//Aggiunta alberghi
		Albergo setar = new Albergo(1,"Setar Hotel");
		Albergo tHotel = new Albergo(2, "THotel");
		
		//Aggiunta Stanze prenotabili
		setar.aggiungiStanzaPrenotabile(1,"Singola", 50.00);
		setar.aggiungiStanzaPrenotabile(2,"Royal Matrimoniale", 150.00);
		tHotel.aggiungiStanzaPrenotabile(1,"Singola",70.00);
		tHotel.aggiungiStanzaPrenotabile(2,"Doppia",100.00);
		tHotel.aggiungiStanzaPrenotabile(3,"Matrimoniale",120.00);
		
		//Verifichiamo che le stanze siano state inserite
		assertEquals(2,setar.getNumeroStanzePrenotabili());
		assertEquals(3,tHotel.getNumeroStanzePrenotabili());
		
		//Informazioni
		GestoreInformazioni gestoreInfo = new GestoreInformazioni();
		gestoreInfo.aggiungiInformazione(1,"Setar Hotel, bello!!");
		gestoreInfo.aggiungiInformazione(2,"THotel, ultrabello!");
		gestoreInfo.aggiungiInformazione(1,1,"La stanza è confortevole");
		gestoreInfo.aggiungiInformazione(2,1,"La Stanza è comoda");
		
		// Verifichiamo che le informazioni siano state inserite
		assertEquals(4,AggregatoreInformazioni.getInstance().getInfo().size());
		
		//Verifichiamo anche che le informazioni siano corrette
		assertEquals("Setar Hotel, bello!!",AggregatoreInformazioni.getInstance().visualizzaInfo(1));
		assertEquals("La stanza è confortevole",AggregatoreInformazioni.getInstance().visualizzaInfo(1,1));
		
		//Vogliamo prenotare una stanza al THotel, per l'esattezza la matrimoniale, che ha id 3
		boolean prenotazione1 = GestoreStanze.getInstance().prenotaStanza(tHotel, "Daniele", 3);
		assertTrue(prenotazione1);
		//Perciò il numero delle stanze prenotabili dovrebbe essere sceso
		assertEquals(2, tHotel.getNumeroStanzePrenotabili());
		
		//Aggiungiamo un'altra prenotazione
		boolean prenotazione3 =  GestoreStanze.getInstance().prenotaStanza(setar, "Daniele", 2);
		assertTrue(prenotazione3);
		assertEquals(1, setar.getNumeroStanzePrenotabili());
		
		//Annulliamo la prima prenotazione
		boolean annullamento = GestoreStanze.getInstance().annullaPrenotazione(tHotel, "Daniele", 3);
		assertTrue(annullamento);
		assertEquals(3, tHotel.getNumeroStanzePrenotabili());
		
		//Paghiamo la stanza prenotata da Giulio
		GestorePagamento gestoreP = new GestorePagamento();
		String risultato = gestoreP.pagaStanze(setar, "Daniele");
		assertEquals("Hai pagato 150.0 euro per le stanze",risultato);
	}
}
