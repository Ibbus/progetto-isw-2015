public class Albergo{
	private int idAlbergo;
	private String nome;
	private GestorePrenotazioniAlbergo gestore;

	public Albergo(int idAlbergo, String nome){
		this.idAlbergo = idAlbergo;
		this.nome = nome;
		this.gestore = new GestorePrenotazioniAlbergo();
	}

	public GestorePrenotazioniAlbergo getGestorePrenotazioniAlbergo(){
		return this.gestore;
	}

	public void aggiungiStanzaPrenotabile(int idStanza, String tipo, double prezzo){
		if(gestore.contieneDaPrenotare(idStanza)){
			System.out.println("ID già inserito");
		}
		else{
			gestore.aggiungiDaPrenotare(idStanza, tipo, prezzo);
		}
	}

	public boolean prenota(String nome, int room){
		if(gestore.giaPrenotato(room)){
			return false;
		}
		else{
			return gestore.aggiungiPrenotazione(nome, room);
		}
	}

	public String getNome(){
		return this.nome;
	}

	public int getNumeroStanzePrenotabili(){
		return this.gestore.numeroStanzePrenotabili();
	}

	public boolean annullaPrenotazione(String nome, int idStanza){
		return this.gestore.annullaPrenotazione(nome, idStanza);
	}

	public Stanza getStanzePrenotate(String nome){
		return this.gestore.getStanzePrenotate(nome);
	}
}
