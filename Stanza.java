public class Stanza {
    private int IDStanza;
    private String tipologia;
    private double prezzo;
    
	public Stanza(int id,String tipo,double prezzo){
		this.IDStanza = id;
		this.tipologia = tipo;
		this.prezzo = prezzo;
	}	

    public int getId(){
        return this.IDStanza;
    }
    
    public String getTipologia(){
        return this.tipologia;
    }
    
    public double getPrezzo(){
        return this.prezzo;
    }
}

