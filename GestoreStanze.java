import java.util.*;

public class GestoreStanze{
	public Hashtable<Albergo,ArrayList<Integer>> stanzePrenotate;
	private static GestoreStanze instance;
	
	private GestoreStanze(){
		stanzePrenotate = new Hashtable<Albergo,ArrayList<Integer>>();
	}

	public static GestoreStanze getInstance(){
		if(instance == null){
			instance = new GestoreStanze();
		}
		return instance;
	}

	public boolean prenotaStanza(Albergo albergo, String nominativo, int idStanza){
		if(albergo.prenota(nominativo, idStanza)){
			if(stanzePrenotate.containsKey(albergo)){
				ArrayList<Integer> prenotazioni = stanzePrenotate.get(albergo);
				prenotazioni.add(idStanza);
				stanzePrenotate.remove(albergo);
				stanzePrenotate.put(albergo,prenotazioni);
			}
			else{
				ArrayList<Integer> prenotata = new ArrayList<Integer>();
				prenotata.add(idStanza);
				stanzePrenotate.put(albergo, prenotata);
			}
			return true;
		}
		else{
			System.out.println("Stanza non disponibile");
			return false;
		}
	}

	public boolean annullaPrenotazione(Albergo albergo, String nominativo, int idStanza){
		if(albergo.annullaPrenotazione(nominativo, idStanza)){
			ArrayList<Integer> prenotazioni = stanzePrenotate.get(albergo);
			prenotazioni.remove((Integer)idStanza);
			stanzePrenotate.remove(albergo);
			stanzePrenotate.put(albergo,prenotazioni);
			return true;
		}
		return false;
	}
}
